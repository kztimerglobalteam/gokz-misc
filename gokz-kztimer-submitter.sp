#include <sourcemod>

#include <gokz>

#include <gokz/core>

#pragma newdecls required
#pragma semicolon 1



// Submit KZTimer times completed in GOKZ to KZTimer local database

public Plugin myinfo = 
{
	name = "GOKZ KZTimer Time Submitter", 
	author = "DanZay", 
	description = "GOKZ KZTimer Time Submitter", 
	version = "1.0.0", 
	url = "https://bitbucket.org/kztimerglobalteam/gokz"
};

Database gH_DB = null;
char gC_CurrentMap[64];
bool gB_HasPBTime[MAXPLAYERS + 1];
float gF_PBTime[MAXPLAYERS + 1];
bool gB_HasPBTimePro[MAXPLAYERS + 1];
float gF_PBTimePro[MAXPLAYERS + 1];

char sql_select_playertimes[] = 
"SELECT runtime, runtimepro FROM playertimes WHERE steamid='%s' AND mapname='%s'";
char sql_insert_playertimes_tp[] = 
"INSERT INTO playertimes (name, runtime, teleports, steamid, mapname) VALUES('%s', '%f', '%d', '%s', '%s')";
char sql_insert_playertimes_pro[] = 
"INSERT INTO playertimes (name, runtimepro, steamid, mapname) VALUES('%s', '%f', '%s', '%s')";
char sql_update_playertimes_tp[] = 
"UPDATE playertimes SET name='%s', runtime='%f', teleports='%d' WHERE steamid='%s' AND mapname='%s'";
char sql_update_playertimes_pro[] = 
"UPDATE playertimes SET name='%s', runtimepro='%f' WHERE steamid='%s' AND mapname='%s'";



public void OnAllPluginsLoaded()
{
	char error[255];
	gH_DB = SQL_Connect("kztimer", true, error, sizeof(error));
	if (gH_DB == null)
	{
		SetFailState("Database connection failed: %s", error);
	}
}

public void OnMapStart()
{
	GetCurrentMap(gC_CurrentMap, sizeof(gC_CurrentMap));
	// Get just the gC_CurrentMap name (e.g. remove workshop/id/ prefix)
	char mapPieces[5][64];
	int lastPiece = ExplodeString(gC_CurrentMap, "/", mapPieces, sizeof(mapPieces), sizeof(mapPieces[]));
	FormatEx(gC_CurrentMap, sizeof(gC_CurrentMap), "%s", mapPieces[lastPiece - 1]);
	String_ToLower(gC_CurrentMap, gC_CurrentMap, sizeof(gC_CurrentMap));
}

public void OnClientPostAdminCheck(int client)
{
	if (IsFakeClient(client))
	{
		return;
	}
	
	char query[1024], steamid[32];
	Transaction txn = SQL_CreateTransaction();
	
	GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	
	// Select times
	FormatEx(query, sizeof(query), sql_select_playertimes, steamid, gC_CurrentMap);
	txn.AddQuery(query);
	
	SQL_ExecuteTransaction(gH_DB, txn, DB_TxnSuccess_SelectTimes, DB_TxnFailure_Generic, GetClientUserId(client), DBPrio_High);
}

public void DB_TxnSuccess_SelectTimes(Handle db, int userid, int numQueries, Handle[] results, any[] queryData)
{
	int client = GetClientOfUserId(userid);
	
	if (!IsValidClient(client))
	{
		return;
	}
	
	if (SQL_GetRowCount(results[0]) == 0)
	{
		gB_HasPBTime[client] = false;
		gB_HasPBTimePro[client] = false;
	}
	else if (SQL_FetchRow(results[0]))
	{
		float pbTime = SQL_FetchFloat(results[0], 0);
		if (pbTime < 0.0)
		{
			gB_HasPBTime[client] = false;
		}
		else
		{
			gB_HasPBTime[client] = true;
			gF_PBTime[client] = pbTime;
		}
		
		float pbTimePro = SQL_FetchFloat(results[0], 1);
		if (pbTimePro < 0.0)
		{
			gB_HasPBTimePro[client] = false;
		}
		else
		{
			gB_HasPBTimePro[client] = true;
			gF_PBTimePro[client] = pbTimePro;
		}
	}
}

public void GOKZ_OnTimerEnd_Post(int client, int course, float time, int teleportsUsed)
{
	if (course != 0 || GOKZ_GetOption(client, Option_Mode) != Mode_KZTimer)
	{
		return;
	}
	
	bool queryPending = false;
	char query[1024], steamid[32], name[MAX_NAME_LENGTH * 2 + 1];
	
	GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	GetClientName(client, name, sizeof(name));
	SQL_EscapeString(gH_DB, name, name, sizeof(name));
	
	int timeType = GOKZ_GetTimeType(teleportsUsed);
	
	if (!gB_HasPBTime[client] && !gB_HasPBTimePro[client])
	{
		if (timeType == TimeType_Pro)
		{
			FormatEx(query, sizeof(query), sql_insert_playertimes_pro, name, time, steamid, gC_CurrentMap);
			gB_HasPBTimePro[client] = true;
			gF_PBTimePro[client] = time;
			queryPending = true;
		}
		else if (timeType == TimeType_Nub)
		{
			FormatEx(query, sizeof(query), sql_insert_playertimes_tp, name, time, teleportsUsed, steamid, gC_CurrentMap);
			gB_HasPBTime[client] = true;
			gF_PBTime[client] = time;
			queryPending = true;
		}
	}
	else if (timeType == TimeType_Pro && (!gB_HasPBTimePro[client] || time < gF_PBTimePro[client]))
	{
		FormatEx(query, sizeof(query), sql_update_playertimes_pro, name, time, steamid, gC_CurrentMap);
		gB_HasPBTimePro[client] = true;
		gF_PBTimePro[client] = time;
		queryPending = true;
	}
	else if (timeType == TimeType_Nub && (!gB_HasPBTime[client] || time < gF_PBTime[client]))
	{
		FormatEx(query, sizeof(query), sql_update_playertimes_tp, name, time, teleportsUsed, steamid, gC_CurrentMap);
		gB_HasPBTime[client] = true;
		gF_PBTime[client] = time;
		queryPending = true;
	}
	
	if (queryPending)
	{
		Transaction txn = SQL_CreateTransaction();
		txn.AddQuery(query);
		SQL_ExecuteTransaction(gH_DB, txn, _, DB_TxnFailure_Generic, _, DBPrio_High);
	}
}

public void DB_TxnFailure_Generic(Handle db, any data, int numQueries, const char[] error, int failIndex, any[] queryData)
{
	SetFailState("Database transaction error: %s", error);
} 