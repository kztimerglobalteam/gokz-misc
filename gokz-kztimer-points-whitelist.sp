#include <sourcemod>

#include <gokz>

#include <gokz/core>

#pragma newdecls required
#pragma semicolon 1



// Block players from connecting by dependent on number of points in KZTimer local database

public Plugin myinfo = 
{
	name = "GOKZ KZTimer Points Whitelist", 
	author = "DanZay", 
	description = "GOKZ KZTimer Points Whitelist", 
	version = "1.0.0", 
	url = "https://bitbucket.org/kztimerglobalteam/gokz"
};

Database gH_DB = null;
ConVar gokz_kztwl_points_required;
char sql_select_points[] = 
"SELECT points FROM playerrank WHERE steamid='%s'";



public void OnPluginStart()
{
	gokz_kztwl_points_required = CreateConVar("gokz_kztwl_points_required", "10000", "KZTimer points required to be allowed to join the server.");
	AutoExecConfig(true, "gokz-kztimer-points-whitelist", "sourcemod/gokz");
}

public void OnAllPluginsLoaded()
{
	char error[255];
	gH_DB = SQL_Connect("kztimer", true, error, sizeof(error));
	if (gH_DB == null)
	{
		SetFailState("Database connection failed: %s", error);
	}
}

public void OnClientPostAdminCheck(int client)
{
	if (IsFakeClient(client))
	{
		return;
	}
	
	char query[1024], steamid[32];
	Transaction txn = SQL_CreateTransaction();
	
	GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	
	// Select times
	FormatEx(query, sizeof(query), sql_select_points, steamid);
	txn.AddQuery(query);
	
	SQL_ExecuteTransaction(gH_DB, txn, DB_TxnSuccess_SelectPoints, DB_TxnFailure_Generic, GetClientUserId(client), DBPrio_High);
}

public void DB_TxnSuccess_SelectPoints(Handle db, int userid, int numQueries, Handle[] results, any[] queryData)
{
	int client = GetClientOfUserId(userid);
	
	if (!IsValidClient(client))
	{
		return;
	}
	
	if (SQL_GetRowCount(results[0]) == 0)
	{
		BlockPlayer(client);
	}
	else if (!SQL_FetchRow(results[0]))
	{
		BlockPlayer(client);
	}
	else
	{
		if (SQL_FetchInt(results[0], 0) < gokz_kztwl_points_required.IntValue)
		{
			BlockPlayer(client);
		}
	}
}

void BlockPlayer(int client)
{
	// Allow admins/VIP
	int accessFlags = GetUserFlagBits(client);
	if (accessFlags & ADMFLAG_RESERVATION || accessFlags & ADMFLAG_GENERIC || accessFlags & ADMFLAG_ROOT)
	{
		return;
	}
	
	KickClient(client, "You don't fulfill the criteria to play on this server. You must be VIP or have %d points", gokz_kztwl_points_required.IntValue);
}

public void DB_TxnFailure_Generic(Handle db, any data, int numQueries, const char[] error, int failIndex, any[] queryData)
{
	SetFailState("Database transaction error: %s", error);
} 